package me.tringlle.randomitem;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;

public class Main extends JavaPlugin implements Listener {
	
	@Override
	public void onEnable() {
		this.saveDefaultConfig();
		this.getServer().getPluginManager().registerEvents(this, this);
	}
	@Override
	public void onDisable() {
		
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(label.equals("randomblock")) {
			if (!sender.hasPermission("randomblock.admin.reload")) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cR&6a&en&ad&9o&dm &cB&6l&eo&ac&9k &7>> &cYou do not have permission to use this command. &7&o(randomblock.admin.reload"));
				return true;
			}
			if (args.length == 0) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cR&6a&en&ad&9o&dm &cB&6l&eo&ac&9k"));
				sender.sendMessage("");
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/randomblock reload"));
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "/randomblock help"));
				sender.sendMessage("");
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cR&6a&en&ad&9o&dm &cB&6l&eo&ac&9k"));
			}
			if (args.length > 0 ) {
				if (args[0].equalsIgnoreCase("reload")) {
					this.reloadConfig();
					
					
					for (String msg : this.getConfig().getStringList("reload.message")) {
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
								msg));
					}
					this.reloadConfig();
				}
				else if (args[0].equalsIgnoreCase("help")) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cR&6a&en&ad&9o&dm &cB&6l&eo&ac&9k"));
					sender.sendMessage("");
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/randomblock reload"));
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "/randomblock help"));
					sender.sendMessage("");
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cR&6a&en&ad&9o&dm &cB&6l&eo&ac&9k"));
				}
			}
		}
		
		return false;
	}
	
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		this.getConfig().getConfigurationSection("blocks").getKeys(false).forEach(key -> {
			// dirt
			if (key.equalsIgnoreCase(event.getBlock().getType().toString())) {
				ItemStack[] items = new ItemStack[this.getConfig().getStringList("blocks." + key).size()];
				ItemStack item = null;
				int position = 0;
				Random r = new Random();
				for (String i: this.getConfig().getStringList("blocks." + key)) {
					try {
						item = new ItemStack(Material.matchMaterial(i), r.nextInt(16) + 1);
					} catch(Exception e) {
						item= new ItemStack(Material.matchMaterial(key));
					}
					items[position] = item;
					position++;
					
				}
				int num = r.nextInt(items.length);
				event.setDropItems(false);
				World world = event.getPlayer().getWorld();
				world.dropItemNaturally(event.getBlock().getLocation(), items[num]);
			}
		});
	}

}
